#include "BST.h"
#include <fstream>
#include <iostream>

template<typename T>
BST<T>::BST() : root(nullptr) {}

template<typename T>
BST<T>::BST(const string& fileName) : root(nullptr)
{
	ifstream inFile(fileName);
	if (!inFile)
	{
		cerr << "Cannot open input file.\n";
		system("pause");
		exit(0);
	}

	T data;
	inFile >> data;
	while (data != -999)
	{
		insert(data);
		inFile >> data;
	}

	inFile.close();
}

template <typename T>
BST<T>::~BST()
{}

template <typename T>
void BST<T>::recursiveInorder() const
{
	if (root == nullptr)
		cerr << "There is no tree.";
	else
	{
		recursiveInorder(root);
	}
}

template <typename T>
void BST<T>::recursiveInorder(const Node<T>* p) const
{
	if (p != nullptr)
	{
		recursiveInorder(p->left);
		cout << p->data << " ";
		recursiveInorder(p->right);
	}
}

template <typename T>
void BST<T>::recursivePostorder() const
{
	if (root == nullptr)
		cerr << "There is no tree.";
	else
	{
		recursivePostorder(root);
	}
}

template <typename T>
void BST<T>::recursivePostorder(const Node<T>* p) const
{
	if (p != nullptr)
	{
		recursivePostorder(p->left);
		recursivePostorder(p->right);
		cout << p->data << " ";
	}
}

template <typename T>
void BST<T>::recursivePreorder() const
{
	if (root == nullptr)
		cerr << "There is no tree.";
	else
	{
		recursivePreorder(root);
	}
}

template <typename T>
void BST<T>::recursivePreorder(const Node<T>* p) const
{
	if (p != nullptr)
	{
		cout << p->data << " ";
		recursivePreorder(p->left);
		recursivePreorder(p->right);
	}
}

template<typename T>
void BST<T>::morrisPostorder() const
{
	Node<T>* dummy = new Node<T>;
	dummy->left = root;

	Node<T>* postTemp = nullptr;
	Node<T>* postTempPrev = nullptr;
	Node<T>* postTempMiddle = nullptr;

	Node<T> *parent = dummy;
	Node<T> *temp = nullptr;

	while (parent != nullptr)
	{
		if (parent->left == nullptr)
		{
			parent = parent->right;
		}
		else
		{
			temp = parent->left;
			while (temp->right != nullptr && temp->right != parent)
			{
				temp = temp->right;
			}
			if (temp->right == nullptr)
			{
				temp->right = parent;
				parent = parent->left;
			}
			else
			{
				postTempPrev = parent;
				postTemp = parent->left;

				while (postTemp != parent)
				{
					postTempMiddle = postTemp->right;
					postTemp->right = postTempPrev;
					postTempPrev = postTemp;
					postTemp = postTempMiddle;
				}

				while (postTempPrev != parent)
				{
					visit(postTempPrev);
					postTempMiddle = postTempPrev->right;
					postTempPrev->right = postTemp;
					postTemp = postTempPrev;
					postTempPrev = postTempMiddle;
				}
				temp->right = nullptr;
				parent = parent->right;
			}
		}
	}
	delete dummy;
	dummy = nullptr;
}

template <typename T>
void BST<T>::morrisPreorder() const
{
	Node<T> *parent = root;
	Node<T> *temp = nullptr;

	while (parent != nullptr)
	{
		if (parent->left == nullptr)
		{
			visit(parent);
			parent = parent->right;
		}
		else
		{
			temp = parent->left;
			while (temp->right != nullptr && temp->right != parent)
			{
				temp = temp->right;
			}
			if (temp->right == nullptr)
			{
				visit(parent);
				temp->right = parent;
				parent = parent->left;
			}
			else
			{
				temp->right = nullptr;
				parent = parent->right;
			}
		}
	}
}

template <typename T>
void BST<T>::morrisInorder() const
{
	Node<T> *parent = root;
	Node<T> *temp = nullptr;

	while (parent != nullptr)
	{
		if (parent->left == nullptr)
		{
			visit(parent);
			parent = parent->right;
		}
		else
		{
			temp = parent->left;
			while (temp->right != nullptr && temp->right != parent)
			{
				temp = temp->right;
			}
			if (temp->right == nullptr)
			{
				temp->right = parent;
				parent = parent->left;
			}
			else
			{
				visit(parent);
				temp->right = nullptr;
				parent = parent->right;
			}
		}
	}
}

template<typename T>
void BST<T>::insert(const T& data)
{
	if (root != nullptr)
	{
		Node<T>* temp = root;
		Node<T>* prev = nullptr;

		while (temp != nullptr)
		{
			prev = temp;
			if (data == temp->data)
			{
				cerr << "No duplicate data nodes are allowed.\n";
				temp = nullptr;
			}
			else if (data > temp->data)
			{
				temp = temp->right;
			}
			else
			{
				temp = temp->left;
			}
		}
		if (data != prev->data)
		{
			Node<T>* newNode = new Node<T>(data);
			if (data > prev->data)
			{
				prev->right = newNode;
			}
			else
			{
				prev->left = newNode;
			}
		}

	}
	else
	{
		root = new Node<T>(data);
	}
}

template<typename T>
void BST<T>::printRoot()
{
	cout << root->data;
}

template<typename T>
void BST<T>::visit(const Node<T>* p) const
{
	cout << p->data << " ";
}