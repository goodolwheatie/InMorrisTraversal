#ifndef BST_H
#define BST_H

#include <string>

using namespace std;

template <typename T>
class Node
{
	template<typename T>
	friend class BST;
public:
	Node() : data(0), left(nullptr), right(nullptr) {}
	Node(const T& data) : data(data), left(nullptr), right(nullptr) {}
	~Node() {}

private:
	T data;
	Node<T> *left, *right;

};

template <typename T>
class BST
{
public:
	BST();
	BST(const string&);
	~BST();
	void recursiveInorder() const;
	void recursiveInorder(const Node<T>* p) const;
	void recursivePostorder() const;
	void recursivePostorder(const Node<T>* p) const;
	void recursivePreorder() const;
	void recursivePreorder(const Node<T>* p) const;

	void morrisPostorder() const;
	void morrisPreorder() const;
	void morrisInorder() const;
	void insert(const T& data);
	void printRoot();

private:
	void visit(const Node<T>* p) const;
	Node<T>* root;

};

#endif // !BST_H
