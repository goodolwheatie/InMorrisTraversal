#include "BST.h"
#include "BST.cpp"
#include <iostream>

int main()
{
	BST<int> myBST("data_int_1.txt");
	cout << "Printing Root: ";
	myBST.printRoot();
	cout << endl << endl;

	cout << "Recursive\n";
	cout << "Inorder Traversal: ";
	myBST.recursiveInorder();
	cout << endl << endl;

	cout << "Morris\n";
	cout << "Inorder Traversal: ";
	myBST.morrisInorder();

	cout << endl << endl;
	cout << "Recursive\n";
	cout << "Post-Order Traversal: ";
	myBST.recursivePostorder();

	cout << endl << endl;
	cout << "Morris\n";
	cout << "Post-Order Traversal: ";
	myBST.morrisPostorder();
	
	cout << endl << endl;
	cout << "Recursive\n";
	cout << "Pre-Order Traversal: ";
	myBST.recursivePreorder();

	cout << endl << endl;
	cout << "Morris\n";
	cout << "Pre-Order Traversal: ";
	myBST.morrisPreorder();

	cout << endl << endl;
	system("pause");
	return 0;
}